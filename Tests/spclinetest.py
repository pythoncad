#
# Copyright (c) 2002, Art Haas
#
#
# test single point construction lines
#

import unittest
import math
import random

import Generic.point as point
import Generic.hcline as hcline
import Generic.vcline as vcline
import Generic.acline as acline

def absdiff(testval, goodval):
    return abs(testval - goodval)

seed = 100
random.seed(seed)

class HCLineTestCase(unittest.TestCase):
    def setUp(self):
        self.p1 = point.Point(0,0)
        self.p2 = point.Point(0,10)
        self.p3 = point.Point(0,-10)
        self.h1 = hcline.HCLine(self.p1)
        self.h2 = hcline.HCLine(self.p2)
        self.h3 = hcline.HCLine(self.p3)

    def testCreation(self):
        self.failUnless(self.h1.getLocation() is self.p1)
        self.failUnless(self.h2.getLocation() is self.p2)
        self.failUnless(self.h3.getLocation() is self.p3)

    def testComparison(self):
        self.failUnless(self.h1 == self.h1)
        self.failUnless(self.h2 > self.h1)
        self.failUnless(self.h3 < self.h1)

        self.failUnless(self.h2 == self.h2)
        self.failUnless(self.h3 < self.h2)

        self.failUnless(self.h3 == self.h3)

        temp = hcline.HCLine((10,0))
        self.failUnless(self.h1 == temp and temp == self.h1)

    def testPointMapping(self):
        p = self.h1.mapPoint((5,0))
        self.failUnless(p == (5,0))
        p = self.h1.mapPoint((5,0.001))
        self.failUnless(p is None)
        p = self.h1.mapPoint((5,0.001), 0.001)
        self.failUnless(p == (5,0))
        p = self.h1.mapPoint((5,-0.001))
        self.failUnless(p is None)
        p = self.h1.mapPoint((5,-0.001), 0.001)
        self.failUnless(p == (5,0))

class HCLineTreeTestCase(unittest.TestCase):
    def setUp(self):
        self.tree = hcline.HCLineTree()
        self.p1 = point.Point(0,0)
        self.p2 = point.Point(0,10)
        self.p3 = point.Point(0,-10)
        self.h1 = hcline.HCLine(self.p1)
        self.h2 = hcline.HCLine(self.p2)
        self.h3 = hcline.HCLine(self.p3)

    def testStorage(self):
        self.tree.store(self.h1)
        self.failUnless(len(self.tree) == 1)
        self.failUnless(self.h1 in self.tree)
        self.tree.store(self.h2)
        self.failUnless(len(self.tree) == 2)
        self.failUnless(self.h2 in self.tree)
        self.tree.store(self.h3)
        self.failUnless(len(self.tree) == 3)
        self.failUnless(self.h3 in self.tree)

        temp = hcline.HCLine((10,0))
        self.failUnless(temp in self.tree)

        for i in xrange(1000):
            x = -100.0 + (random.random() * 200.0)
            y = -100.0 + (random.random() * 200.0)
            p = point.Point(x,y)
            hcl = hcline.HCLine(p)
            self.tree.store(hcl)
        self.validateTree()

    def validateTree(self):
        for i in xrange(1, len(self.tree)):
            hp = self.tree[i-1]
            hi = self.tree[i]
            hpx, hpy = hp.getLocation().getCoords()
            hix, hiy = hi.getLocation().getCoords()
            self.failUnless(hpy <= hiy)

    def testBinscan(self):
        self.tree.store(self.h1)
        self.tree.store(self.h2)
        self.tree.store(self.h3)
        sh = self.tree.binscan(self.h1)
        self.failUnless(sh is self.h1)
        sh = self.tree.binscan(self.h2)
        self.failUnless(sh is self.h2)
        sh = self.tree.binscan(self.h3)
        self.failUnless(sh is self.h3)

        temp = hcline.HCLine((10,0))
        sh = self.tree.binscan(temp)
        self.failUnless(sh is self.h1)

        temp = hcline.HCLine((1,1))
        sh = self.tree.binscan(temp)
        self.failUnless(sh is None)

    def testScan(self):
        self.tree.store(self.h1)
        self.tree.store(self.h2)
        self.tree.store(self.h3)
        sh = self.tree.scan(self.h1)
        self.failUnless(sh is self.h1)
        sh = self.tree.scan(self.h2)
        self.failUnless(sh is self.h2)
        sh = self.tree.scan(self.h3)
        self.failUnless(sh is self.h3)

        temp = hcline.HCLine((10,0))
        sh = self.tree.scan(temp)
        self.failUnless(sh is self.h1)

        temp = hcline.HCLine((1,1))
        sh = self.tree.scan(temp)
        self.failUnless(sh is None)

    def testRemove(self):
        self.tree.store(self.h1)
        self.tree.store(self.h2)
        self.tree.store(self.h3)
        self.tree.remove(self.h1)
        self.failUnless(len(self.tree) == 2)
        self.failIf(self.h1 in self.tree)
        self.tree.remove(self.h2)
        self.failUnless(len(self.tree) == 1)
        self.failIf(self.h2 in self.tree)
        self.tree.remove(self.h3)
        self.failIf(len(self.tree))
        self.failIf(self.h3 in self.tree)

    def testPointMapping(self):
        self.tree.store(self.h1)
        self.tree.store(self.h2)
        self.tree.store(self.h3)
        hits = self.tree.mapPoint((10,10))
        self.failUnless(len(hits) == 1)
        self.failUnless(hits[0][0] is self.h2)
        self.failUnless(hits[0][1] == (10,10))
        hits = self.tree.mapPoint((10,10.001))
        self.failIf(len(hits))
        hits = self.tree.mapPoint((10,10.001),0.001)
        self.failUnless(len(hits) == 1)
        self.failUnless(hits[0][0] is self.h2)
        self.failUnless(hits[0][1] == (10,10))
        hits = self.tree.mapPoint((10,9.999))
        self.failIf(len(hits))
        hits = self.tree.mapPoint((10,9.999),0.001)
        self.failUnless(len(hits) == 1)
        self.failUnless(hits[0][0] is self.h2)
        self.failUnless(hits[0][1] == (10,10))
        
class VCLineTestCase(unittest.TestCase):
    def setUp(self):
        self.p1 = point.Point(0,0)
        self.p2 = point.Point(10,0)
        self.p3 = point.Point(-10,0)
        self.v1 = vcline.VCLine(self.p1)
        self.v2 = vcline.VCLine(self.p2)
        self.v3 = vcline.VCLine(self.p3)

    def testCreation(self):
        self.failUnless(self.v1.getLocation() is self.p1)
        self.failUnless(self.v2.getLocation() is self.p2)
        self.failUnless(self.v3.getLocation() is self.p3)

    def testComparison(self):
        self.failUnless(self.v1 == self.v1)
        self.failUnless(self.v2 > self.v1)
        self.failUnless(self.v3 < self.v1)

        self.failUnless(self.v2 == self.v2)
        self.failUnless(self.v3 < self.v2)

        self.failUnless(self.v3 == self.v3)

        temp = vcline.VCLine((0,10))

        self.failUnless(self.v1 == temp and temp == self.v1)

    def testPointMapping(self):
        p = self.v1.mapPoint((0,5))
        self.failUnless(p == (0,5))
        p = self.v1.mapPoint((0.001,5))
        self.failUnless(p is None)
        p = self.v1.mapPoint((0.001,5), 0.001)
        self.failUnless(p == (0,5))
        p = self.v1.mapPoint((-0.001,5))
        self.failUnless(p is None)
        p = self.v1.mapPoint((-0.001,5), 0.001)
        self.failUnless(p == (0,5))

class VCLineTreeTestCase(unittest.TestCase):
    def setUp(self):
        self.tree = vcline.VCLineTree()
        self.p1 = point.Point(0,0)
        self.p2 = point.Point(10,0)
        self.p3 = point.Point(-10,0)
        self.v1 = vcline.VCLine(self.p1)
        self.v2 = vcline.VCLine(self.p2)
        self.v3 = vcline.VCLine(self.p3)

    def testStorage(self):
        self.tree.store(self.v1)
        self.failUnless(len(self.tree) == 1)
        self.failUnless(self.v1 in self.tree)
        self.tree.store(self.v2)
        self.failUnless(len(self.tree) == 2)
        self.failUnless(self.v2 in self.tree)
        self.tree.store(self.v3)
        self.failUnless(len(self.tree) == 3)
        self.failUnless(self.v3 in self.tree)
        
        temp = vcline.VCLine((0,10))
        self.failUnless(temp in self.tree)

        for i in xrange(1000):
            x = -100.0 + (random.random() * 200.0)
            y = -100.0 + (random.random() * 200.0)
            p = point.Point(x,y)
            vcl = vcline.VCLine(p)
            self.tree.store(vcl)
        self.validateTree()

    def validateTree(self):
        for i in xrange(1, len(self.tree)):
            vp = self.tree[i-1]
            vi = self.tree[i]
            vpx, vpy = vp.getLocation().getCoords()
            vix, viy = vi.getLocation().getCoords()
            self.failUnless(vpx <= vix)

    def testBinscan(self):
        self.tree.store(self.v1)
        self.tree.store(self.v2)
        self.tree.store(self.v3)
        sh = self.tree.binscan(self.v1)
        self.failUnless(sh is self.v1)
        sh = self.tree.binscan(self.v2)
        self.failUnless(sh is self.v2)
        sh = self.tree.binscan(self.v3)
        self.failUnless(sh is self.v3)

        temp = vcline.VCLine((0,10))
        sh = self.tree.binscan(temp)
        self.failUnless(sh is self.v1)

        temp = vcline.VCLine((1,1))
        sh = self.tree.binscan(temp)
        self.failUnless(sh is None)

    def testScan(self):
        self.tree.store(self.v1)
        self.tree.store(self.v2)
        self.tree.store(self.v3)
        sh = self.tree.scan(self.v1)
        self.failUnless(sh is self.v1)
        sh = self.tree.scan(self.v2)
        self.failUnless(sh is self.v2)
        sh = self.tree.scan(self.v3)
        self.failUnless(sh is self.v3)

        temp = vcline.VCLine((0,10))
        sh = self.tree.scan(temp)
        self.failUnless(sh is self.v1)

        temp = vcline.VCLine((1,1))
        sh = self.tree.scan(temp)
        self.failUnless(sh is None)

    def testRemove(self):
        self.tree.store(self.v1)
        self.tree.store(self.v2)
        self.tree.store(self.v3)
        self.tree.remove(self.v1)
        self.failUnless(len(self.tree) == 2)
        self.failIf(self.v1 in self.tree)
        self.tree.remove(self.v2)
        self.failUnless(len(self.tree) == 1)
        self.failIf(self.v2 in self.tree)
        self.tree.remove(self.v3)
        self.failIf(len(self.tree))
        self.failIf(self.v3 in self.tree)

    def testPointMapping(self):
        self.tree.store(self.v1)
        self.tree.store(self.v2)
        self.tree.store(self.v3)
        hits = self.tree.mapPoint((10,10))
        self.failUnless(len(hits) == 1)
        self.failUnless(hits[0][0] is self.v2)
        self.failUnless(hits[0][1] == (10,10))
        hits = self.tree.mapPoint((10.001,10))
        self.failIf(len(hits))
        hits = self.tree.mapPoint((10.001,10),0.001)
        self.failUnless(len(hits) == 1)
        self.failUnless(hits[0][0] is self.v2)
        self.failUnless(hits[0][1] == (10,10))
        hits = self.tree.mapPoint((9.999,10))
        self.failIf(len(hits))
        hits = self.tree.mapPoint((9.999,10),0.001)
        self.failUnless(len(hits) == 1)
        self.failUnless(hits[0][0] is self.v2)
        self.failUnless(hits[0][1] == (10,10))

class ACLineTestCase(unittest.TestCase):
    def setUp(self):
        self.p1 = point.Point(0,0)
        self.a1 = acline.ACLine(self.p1, -90)
        self.a2 = acline.ACLine(self.p1, -45)
        self.a3 = acline.ACLine(self.p1, 0)
        self.a4 = acline.ACLine(self.p1, 45)
        self.a5 = acline.ACLine(self.p1, 90)

    def testCreation(self):
        self.failUnless(self.a1.getLocation() is self.p1)
        self.failUnless(absdiff(self.a1.getAngle(), -90.0) < 1e-10)
        self.failUnless(self.a2.getLocation() is self.p1)
        self.failUnless(absdiff(self.a2.getAngle(), -45.0) < 1e-10)
        self.failUnless(self.a3.getLocation() is self.p1)
        self.failUnless(absdiff(self.a3.getAngle(), 0) < 1e-10)
        self.failUnless(self.a4.getLocation() is self.p1)
        self.failUnless(absdiff(self.a4.getAngle(), 45) < 1e-10)
        self.failUnless(self.a5.getLocation() is self.p1)
        self.failUnless(absdiff(self.a5.getAngle(), 90) < 1e-10)

    def testComparison(self):
        self.failUnless(self.a1 == self.a1)
        self.failUnless(self.a1 > self.a2 and self.a2 < self.a1)
        self.failUnless(self.a1 > self.a3 and self.a3 < self.a1)
        self.failUnless(self.a1 > self.a4 and self.a4 < self.a1)
        self.failUnless(self.a1 == self.a5)

        self.failUnless(self.a2 == self.a2)
        self.failUnless(self.a2 < self.a3 and self.a3 > self.a2)
        self.failUnless(self.a2 < self.a4 and self.a4 > self.a2)
        self.failUnless(self.a2 < self.a5 and self.a5 > self.a2)

        self.failUnless(self.a3 == self.a3)
        self.failUnless(self.a3 < self.a4 and self.a4 > self.a3)
        self.failUnless(self.a3 < self.a5 and self.a5 > self.a3)

        self.failUnless(self.a4 == self.a4)
        self.failUnless(self.a4 < self.a5 and self.a5 > self.a4)

        self.failUnless(self.a5 == self.a5)

        temp = acline.ACLine((0,10), 0)
        self.failUnless(self.a3 < temp and temp > self.a3)

        temp = acline.ACLine((0,-10), 0)
        self.failUnless(self.a3 > temp and temp < self.a3)

        temp = acline.ACLine((10,0), 45)
        self.failUnless(self.a4 > temp and temp < self.a4)

        temp = acline.ACLine((10,20), 45)
        self.failUnless(self.a4 < temp and temp > self.a4)

    def testPointMapping(self):
        p = self.a1.mapPoint((0,5))
        self.failUnless(p == (0,5))
        p = self.a1.mapPoint((0.001,5))
        self.failUnless(p is None)
        p = self.a1.mapPoint((0.001,5), 0.001)
        self.failUnless(p == (0,5))
        p = self.a1.mapPoint((-0.001,5))
        self.failUnless(p is None)
        p = self.a1.mapPoint((-0.001,5), 0.001)
        self.failUnless(p == (0,5))

        p = self.a3.mapPoint((5,0))
        self.failUnless(p == (5,0))
        p = self.a3.mapPoint((5,0.001))
        self.failUnless(p is None)
        p = self.a3.mapPoint((5,0.001), 0.001)
        self.failUnless(p == (5,0))
        p = self.a3.mapPoint((5,-0.001))
        self.failUnless(p is None)
        p = self.a3.mapPoint((5,-0.001), 0.001)
        self.failUnless(p == (5,0))

        p = self.a4.mapPoint((5,5))
        self.failUnless(p == (5,5))
        p = self.a4.mapPoint((5.001,4.999))
        self.failUnless(p is None)
        p = self.a4.mapPoint((5.001,4.999),0.001)
        self.failUnless(p == (5,5))
        p = self.a4.mapPoint((4.999,5.001))
        self.failUnless(p is None)
        p = self.a4.mapPoint((4.999,5.001),0.001)
        self.failUnless(p == (5,5))

class ACLineTreeTestCase(unittest.TestCase):
    def setUp(self):
        self.tree = acline.ACLineTree()
        self.p1 = point.Point(0,0)
        self.a1 = acline.ACLine(self.p1, -90)
        self.a2 = acline.ACLine(self.p1, -45)
        self.a3 = acline.ACLine(self.p1, 0)
        self.a4 = acline.ACLine(self.p1, 45)
        self.a5 = acline.ACLine(self.p1, 90)

    def testStorage(self):
        self.tree.store(self.a1)
        self.failUnless(len(self.tree) == 1)
        self.failUnless(self.a1 in self.tree)
        self.tree.store(self.a2)
        self.failUnless(len(self.tree) == 2)
        self.failUnless(self.a2 in self.tree)
        self.tree.store(self.a3)
        self.failUnless(len(self.tree) == 3)
        self.failUnless(self.a3 in self.tree)
        self.tree.store(self.a4)
        self.failUnless(len(self.tree) == 4)
        self.failUnless(self.a4 in self.tree)
        self.failUnless(self.a5 in self.tree) # same as self.a1

        temp = acline.ACLine((0,10), 90)
        self.failUnless(temp in self.tree)
        temp = acline.ACLine((0,10), -90)
        self.failUnless(temp in self.tree)
        temp = acline.ACLine((10,10), 45)
        self.failUnless(temp in self.tree)
        temp = acline.ACLine((10,-10), -45)
        self.failUnless(temp in self.tree)
        temp = acline.ACLine((10,0), 0)
        self.failUnless(temp in self.tree)

        for i in xrange(1000):
            x = -100.0 + (random.random() * 200.0)
            y = -100.0 + (random.random() * 200.0)
            p = point.Point(x,y)
            angle = -90.0 * (random.random() * 180.0)
            acl = acline.ACLine(p, angle)
            self.tree.store(acl)
        self.validateTree()

        p = point.Point(5,5)
        for i in xrange(1000):
            angle = -90.0 * (random.random() * 180.0)
            acl = acline.ACLine(p, angle)
            self.tree.store(acl)
        self.validateTree()

    def validateTree(self):
        dtr = math.pi/180.0
        for i in xrange(1, len(self.tree)):
            ap = self.tree[i-1]
            ai = self.tree[i]
            apx, apy = ap.getLocation().getCoords()
            ap_angle = ap.getAngle()
            ap_vert = False
            if abs(abs(ap_angle) - 90) < 1e-6: # close enough ...
                ap_vert = True
            aix, aiy = ai.getLocation().getCoords()
            ai_angle = ai.getAngle()
            ai_vert = False
            if abs(abs(ai_angle) - 90) < 1e-6: # close enough ...
                ai_vert = True
            self.failIf(ap_vert and not ai_vert)
            if ap_vert and ai_vert:
                self.failUnless(apx <= aix)
            else:
                if not ai_vert:
                    ap_m = math.tan(ap_angle*dtr)
                    ap_b = apy - (ap_m*apx)
                    ai_m = math.tan(ai_angle*dtr)
                    ai_b = aiy - (ai_m*aix)
                    self.failUnless(ap_b <= ai_b)
                    if abs(ap_b - ai_b) < 1e-10:
                        self.failUnless(ap_m <= ai_m)

    def testBinscan(self):
        self.tree.store(self.a1)
        self.tree.store(self.a2)
        self.tree.store(self.a3)
        self.tree.store(self.a4)
        sh = self.tree.binscan(self.a1)
        self.failUnless(sh is self.a1)
        sh = self.tree.binscan(self.a2)
        self.failUnless(sh is self.a2)
        sh = self.tree.binscan(self.a3)
        self.failUnless(sh is self.a3)
        sh = self.tree.binscan(self.a4)
        self.failUnless(sh is self.a4)
        sh = self.tree.binscan(self.a5)
        self.failUnless(sh is self.a1)
        
        temp = acline.ACLine((0,10), 90)
        sh = self.tree.binscan(temp)
        self.failUnless(sh is self.a1)

        temp = acline.ACLine((1,1), 30)
        sh = self.tree.binscan(temp)
        self.failUnless(sh is None)

    def testScan(self):
        self.tree.store(self.a1)
        self.tree.store(self.a2)
        self.tree.store(self.a3)
        self.tree.store(self.a4)
        sh = self.tree.scan(self.a1)
        self.failUnless(sh is self.a1)
        sh = self.tree.scan(self.a2)
        self.failUnless(sh is self.a2)
        sh = self.tree.scan(self.a3)
        self.failUnless(sh is self.a3)
        sh = self.tree.binscan(self.a4)
        self.failUnless(sh is self.a4)
        sh = self.tree.binscan(self.a5)
        self.failUnless(sh is self.a1)

        temp = acline.ACLine((0,10), 90)
        sh = self.tree.scan(temp)
        self.failUnless(sh is self.a1)

        temp = acline.ACLine((1,1), 30)
        sh = self.tree.scan(temp)
        self.failUnless(sh is None)

    def testRemove(self):
        self.tree.store(self.a1)
        self.tree.store(self.a2)
        self.tree.store(self.a3)
        self.tree.store(self.a4)
        self.tree.remove(self.a1)
        self.failUnless(len(self.tree) == 3)
        self.failIf(self.a1 in self.tree)
        self.tree.remove(self.a2)
        self.failUnless(len(self.tree) == 2)
        self.failIf(self.a2 in self.tree)
        self.tree.remove(self.a3)
        self.failUnless(len(self.tree) == 1)
        self.failIf(self.a3 in self.tree)
        self.tree.remove(self.a4)
        self.failIf(len(self.tree))
        self.failIf(self.a4 in self.tree)

    def testPointMapping(self):
        self.tree.store(self.a1)
        self.tree.store(self.a2)
        self.tree.store(self.a3)
        self.tree.store(self.a4)
        hits = self.tree.mapPoint((10,10))
        self.failUnless(len(hits) == 1)
        self.failUnless(hits[0][0] is self.a4)
        self.failUnless(hits[0][1] == (10,10))
        hits = self.tree.mapPoint((10.001,10))
        self.failIf(len(hits))
        hits = self.tree.mapPoint((10.001,10),0.001)
        self.failUnless(len(hits) == 1)
        self.failUnless(hits[0][0] is self.a4)
        self.failUnless(hits[0][1] == (10.0005,10.0005))
        hits = self.tree.mapPoint((9.999,10))
        self.failIf(len(hits))
        hits = self.tree.mapPoint((9.999,10),0.001)
        self.failUnless(len(hits) == 1)
        self.failUnless(hits[0][0] is self.a4)
        self.failUnless(hits[0][1] == (9.9995,9.9995))

        hits = self.tree.mapPoint((10,0))
        self.failUnless(len(hits) == 1)
        self.failUnless(hits[0][0] is self.a3)
        self.failUnless(hits[0][1] == (10,0))
        hits = self.tree.mapPoint((10,0.001))
        self.failIf(len(hits))
        hits = self.tree.mapPoint((10,0.001),0.001)
        self.failUnless(len(hits) == 1)
        self.failUnless(hits[0][0] is self.a3)
        self.failUnless(hits[0][1] == (10,0))
        hits = self.tree.mapPoint((10,-0.001))
        self.failIf(len(hits))
        hits = self.tree.mapPoint((10,-0.001),0.001)
        self.failUnless(len(hits) == 1)
        self.failUnless(hits[0][0] is self.a3)
        self.failUnless(hits[0][1] == (10,0))

        hits = self.tree.mapPoint((0,10))
        self.failUnless(len(hits) == 1)
        self.failUnless(hits[0][0] is self.a1)
        self.failUnless(hits[0][1] == (0,10))
        hits = self.tree.mapPoint((0.001,10))
        self.failIf(len(hits))
        hits = self.tree.mapPoint((0.001,10),0.001)
        self.failUnless(len(hits) == 1)
        self.failUnless(hits[0][0] is self.a1)
        self.failUnless(hits[0][1] == (0,10))
        hits = self.tree.mapPoint((-0.001,10))
        self.failIf(len(hits))
        hits = self.tree.mapPoint((-0.001,10),0.001)
        self.failUnless(len(hits) == 1)
        self.failUnless(hits[0][0] is self.a1)
        self.failUnless(hits[0][1] == (0,10))


if __name__ == "__main__":
    HCLineTestSuite = unittest.TestSuite()
    HCLineTestSuite.addTest(HCLineTestCase("testCreation"))
    HCLineTestSuite.addTest(HCLineTestCase("testComparison"))
    HCLineTestSuite.addTest(HCLineTestCase("testPointMapping"))

    HCLineTreeTestSuite = unittest.TestSuite()
    HCLineTreeTestSuite.addTest(HCLineTreeTestCase("testStorage"))
    HCLineTreeTestSuite.addTest(HCLineTreeTestCase("testBinscan"))
    HCLineTreeTestSuite.addTest(HCLineTreeTestCase("testScan"))
    HCLineTreeTestSuite.addTest(HCLineTreeTestCase("testRemove"))
    HCLineTreeTestSuite.addTest(HCLineTreeTestCase("testPointMapping"))

    VCLineTestSuite = unittest.TestSuite()
    VCLineTestSuite.addTest(VCLineTestCase("testCreation"))
    VCLineTestSuite.addTest(VCLineTestCase("testComparison"))
    VCLineTestSuite.addTest(VCLineTestCase("testPointMapping"))

    VCLineTreeTestSuite = unittest.TestSuite()
    VCLineTreeTestSuite.addTest(VCLineTreeTestCase("testStorage"))
    VCLineTreeTestSuite.addTest(VCLineTreeTestCase("testBinscan"))
    VCLineTreeTestSuite.addTest(VCLineTreeTestCase("testScan"))
    VCLineTreeTestSuite.addTest(VCLineTreeTestCase("testRemove"))
    VCLineTreeTestSuite.addTest(VCLineTreeTestCase("testPointMapping"))

    ACLineTestSuite = unittest.TestSuite()
    ACLineTestSuite.addTest(ACLineTestCase("testCreation"))
    ACLineTestSuite.addTest(ACLineTestCase("testComparison"))
    ACLineTestSuite.addTest(ACLineTestCase("testPointMapping"))

    ACLineTreeTestSuite = unittest.TestSuite()
    ACLineTreeTestSuite.addTest(ACLineTreeTestCase("testStorage"))
    ACLineTreeTestSuite.addTest(ACLineTreeTestCase("testBinscan"))
    ACLineTreeTestSuite.addTest(ACLineTreeTestCase("testScan"))
    ACLineTreeTestSuite.addTest(ACLineTreeTestCase("testRemove"))
    ACLineTreeTestSuite.addTest(ACLineTreeTestCase("testPointMapping"))

    unittest.main()
        
