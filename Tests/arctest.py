#
# Copyright (c) 2002, Art Haas
#
#
# test arcs
#

import unittest
import math
import random

import Generic.point as point
import Generic.arc as arc

def absdiff(testval, goodval):
    return abs(testval - goodval)

class ArcTestCase(unittest.TestCase):
    def setUp(self):
        self.p1 = point.Point(0,0)
        self.p2 = point.Point(10,0)
        self.a1 = arc.Arc(self.p1, 5, 0, 180)
        self.a2 = arc.Arc(self.p2, 5, 0, 180)
        self.a3 = arc.Arc(self.p1, 10, 0, 180)
        self.a4 = arc.Arc(self.p2, 10, 0, 180)

    def testCreation(self):
        self.failUnless(self.a1.center is self.p1 and self.a1.radius == 5)
        self.failUnless(self.a1.start_angle == 0 and self.a1.end_angle == 180)
        self.failUnless(self.a2.center is self.p2 and self.a2.radius == 5)
        self.failUnless(self.a2.start_angle == 0 and self.a2.end_angle == 180)
        self.failUnless(self.a3.center is self.p1 and self.a3.radius == 10)
        self.failUnless(self.a3.start_angle == 0 and self.a3.end_angle == 180)
        self.failUnless(self.a4.center is self.p2 and self.a4.radius == 10)
        self.failUnless(self.a4.start_angle == 0 and self.a4.end_angle == 180)

    def testComparison(self):
        self.failUnless(self.a1 == self.a1)
        self.failUnless(self.a1 < self.a2)
        self.failUnless(self.a1 > self.a3)
        self.failUnless(self.a1 < self.a4)

        self.failUnless(self.a2 == self.a2)
        self.failUnless(self.a2 > self.a3)
        self.failUnless(self.a2 > self.a4)

        self.failUnless(self.a3 == self.a3)
        self.failUnless(self.a3 < self.a4)

        self.failUnless(self.a4 == self.a4)

        temp = arc.Arc(self.p1, self.a1.radius + 0.001,
                       self.a1.start_angle, self.a1.end_angle)

        self.failUnless(self.a1 > temp)

        temp = arc.Arc(self.p1, self.a1.radius - 0.001,
                       self.a1.start_angle, self.a1.end_angle)

        self.failUnless(self.a1 < temp)
        
        temp = arc.Arc(self.p1, self.a1.radius,
                       self.a1.start_angle + 0.001, self.a1.end_angle)

        self.failUnless(self.a1 < temp)

        temp = arc.Arc(self.p1, self.a1.radius,
                       self.a1.start_angle - 0.001, self.a1.end_angle)

        self.failUnless(self.a1 < temp)

        temp = arc.Arc(self.p1, self.a1.radius,
                       self.a1.start_angle, self.a1.end_angle + 0.001)

        self.failUnless(self.a1 < temp)

        temp = arc.Arc(self.p1, self.a1.radius,
                       self.a1.start_angle, self.a1.end_angle - 0.001)

        self.failUnless(self.a1 > temp)

    def testPointMapping(self):
        p = self.a1.mapPoint(self.p1)
        self.failUnless(p is None)
        p = self.a1.mapPoint(self.p2)
        self.failUnless(p is None)
        p = self.a2.mapPoint(self.p1)
        self.failUnless(p is None)
        p = self.a2.mapPoint(self.p2)
        self.failUnless(p is None)
        p = self.a3.mapPoint(self.p1)
        self.failUnless(p is None)
        p = self.a3.mapPoint(self.p2)
        self.failUnless(p == (10,0))
        p = self.a4.mapPoint(self.p1)
        self.failUnless(p == (0,0))
        p = self.a4.mapPoint(self.p2)
        self.failUnless(p is None)

        p = self.a1.mapPoint((0,5))
        self.failUnless(p == (0,5))
        p = self.a1.mapPoint((0,4.999))
        self.failUnless(p is None)
        p = self.a1.mapPoint((0,4.999),0.001)
        self.failUnless(p == (0,5))
        p = self.a1.mapPoint((0,5.001))
        self.failUnless(p is None)
        p = self.a1.mapPoint((0,5.001), 0.001)
        self.failUnless(p == (0,5))

    def testArea(self):
        area = 12.5 * math.pi
        self.failUnless(absdiff(self.a1.area(), area) < 1e-10)
        self.failUnless(absdiff(self.a2.area(), area) < 1e-10)
        area = 50.0 * math.pi
        self.failUnless(absdiff(self.a3.area(), area) < 1e-10)
        self.failUnless(absdiff(self.a4.area(), area) < 1e-10)

    def testLength(self):
        l = 5.0 * math.pi
        self.failUnless(absdiff(self.a1.length(), l) < 1e-10)
        self.failUnless(absdiff(self.a2.length(), l) < 1e-10)
        l = 10.0 * math.pi
        self.failUnless(absdiff(self.a3.length(), l) < 1e-10)
        self.failUnless(absdiff(self.a4.length(), l) < 1e-10)

class ArcTreeTestCase(unittest.TestCase):
    def setUp(self):
        self.tree = arc.ArcTree()
        self.p1 = point.Point(0,0)
        self.p2 = point.Point(10,0)
        self.a1 = arc.Arc(self.p1, 5, 0, 180)
        self.a2 = arc.Arc(self.p2, 5, 0, 180)
        self.a3 = arc.Arc(self.p1, 10, 0, 180)
        self.a4 = arc.Arc(self.p2, 10, 0, 180)

    def testStorage(self):
        self.tree.store(self.a1)
        self.failUnless(len(self.tree) == 1)
        self.failUnless(self.a1 in self.tree)
        self.tree.store(self.a2)
        self.failUnless(len(self.tree) == 2)
        self.failUnless(self.a2 in self.tree)
        self.tree.store(self.a3)
        self.failUnless(len(self.tree) == 3)
        self.failUnless(self.a3 in self.tree)
        self.tree.store(self.a4)
        self.failUnless(len(self.tree) == 4)
        self.failUnless(self.a4 in self.tree)

        seed = 100
        random.seed(seed)
        for i in xrange(1000):
            x = -100.0 + (random.random() * 200.0)
            y = -100.0 + (random.random() * 200.0)
            p = point.Point(x,y)
            r = 1.0 + (random.random() * 50.0)
            sa = 0.0 + (random.random() * 360.0)
            ea = 0.0 + (random.random() * 360.0)
            a = arc.Arc(p, r, sa, ea)
            self.tree.store(a)
        self.validateTree()

        p = point.Point(5,5)
        for i in xrange(1000):
            r = 1.0 + (random.random() * 50.0)
            sa = 0.0 + (random.random() * 360.0)
            ea = 0.0 + (random.random() * 360.0)
            a = arc.Arc(p, r, sa, ea)
            self.tree.store(a)
        self.validateTree()

        r = 12.5
        for i in xrange(1000):
            sa = 0.0 + (random.random() * 360.0)
            ea = 0.0 + (random.random() * 360.0)
            a = arc.Arc(p, r, sa, ea)
            self.tree.store(a)
        self.validateTree()

        sa = 45.0
        for i in xrange(1000):
            ea = 0.0 + (random.random() * 360.0)
            a = arc.Arc(p, r, sa, ea)
            self.tree.store(a)
        self.validateTree()

    def validateTree(self):
        for i in xrange(1, len(self.tree)):
            ap = self.tree[i-1]
            apx, apy = ap.getCenter().getCoords()
            apr = ap.getRadius()
            ai = self.tree[i]
            aix, aiy = ai.getCenter().getCoords()
            air = ai.getRadius()
            ap_xmin = apx - apr
            ap_ymin = apy - apr
            ap_xmax = apx + apr
            ap_ymax = apy + apr
            ai_xmin = aix - air
            ai_ymin = aiy - air
            ai_xmax = aix + air
            ai_ymax = aiy + air
            self.failUnless(ap_xmin <= ai_xmin)
            if abs(ap_xmin - ai_xmin) < 1e-10:
                self.failUnless(ap_ymin <= ai_ymin)
                if abs(ap_ymin - ai_ymin) < 1e-10:
                    self.failUnless(ap_xmax <= ai_xmax)                    
                    if abs(ap_xmax - ai_xmax) < 1e-10:
                        self.failUnless(ap_ymax <= ai_ymax)
                        ap_sa = ap.start_angle
                        ai_sa = ai.start_angle
                        self.failUnless(ap_sa <= ai_sa)
                        if abs(ap_sa - ai_sa) < 1e-10:
                            ap_sweep = ap.getAngle()
                            ai_sweep = ai.getAngle()
                            self.failUnless(ap_sweep <= ai_sweep)


    def testBinscan(self):
        self.tree.store(self.a1)
        self.tree.store(self.a2)
        self.tree.store(self.a3)
        self.tree.store(self.a4)
        sa = self.tree.binscan(self.a1)
        self.failUnless(sa is self.a1)
        sa = self.tree.binscan(self.a2)
        self.failUnless(sa is self.a2)
        sa = self.tree.binscan(self.a3)
        self.failUnless(sa is self.a3)
        sa = self.tree.binscan(self.a4)
        self.failUnless(sa is self.a4)

        temp = arc.Arc(self.p1, self.a1.radius,
                       self.a1.start_angle, self.a1.end_angle)

        sa = self.tree.binscan(temp)
        self.failUnless(sa is self.a1)

    def testScan(self):
        self.tree.store(self.a1)
        self.tree.store(self.a2)
        self.tree.store(self.a3)
        self.tree.store(self.a4)
        sa = self.tree.scan(self.a1)
        self.failUnless(sa is self.a1)
        sa = self.tree.scan(self.a2)
        self.failUnless(sa is self.a2)
        sa = self.tree.scan(self.a3)
        self.failUnless(sa is self.a3)
        sa = self.tree.scan(self.a4)
        self.failUnless(sa is self.a4)

        temp = arc.Arc(self.p1, self.a1.radius,
                       self.a1.start_angle, self.a1.end_angle)

        sa = self.tree.scan(temp)
        self.failUnless(sa is self.a1)

    def testRemove(self):
        self.tree.store(self.a1)
        self.tree.store(self.a2)
        self.tree.store(self.a3)
        self.tree.store(self.a4)
        self.tree.remove(self.a1)
        self.failUnless(len(self.tree) == 3)
        self.failIf(self.a1 in self.tree)
        self.tree.remove(self.a2)
        self.failUnless(len(self.tree) == 2)
        self.failIf(self.a2 in self.tree)
        self.tree.remove(self.a3)
        self.failUnless(len(self.tree) == 1)
        self.failIf(self.a3 in self.tree)
        self.tree.remove(self.a4)
        self.failIf(len(self.tree))
        self.failIf(self.a4 in self.tree)

    def testMapping(self):
        self.tree.store(self.a1)
        self.tree.store(self.a2)
        self.tree.store(self.a3)
        self.tree.store(self.a4)

        hits = self.tree.mapPoint((5,0))
        self.failUnless(len(hits) == 2)
        self.failUnless(hits[0][0] is self.a1 or hits[1][0] is self.a1)
        self.failUnless(hits[0][0] is self.a2 or hits[1][0] is self.a2)
        self.failUnless(hits[0][1] == (5,0))
        self.failUnless(hits[1][1] == (5,0))

        hits = self.tree.mapPoint((5.001,0))
        self.failIf(len(hits))
        hits = self.tree.mapPoint((5.001,0), 0.001)
        self.failUnless(len(hits) == 2)
        self.failUnless(hits[0][0] is self.a1 or hits[1][0] is self.a1)
        self.failUnless(hits[0][0] is self.a2 or hits[1][0] is self.a2)
        self.failUnless(hits[0][1] == (5,0))
        self.failUnless(hits[1][1] == (5,0))

        hits = self.tree.mapPoint((4.999,0))
        self.failIf(len(hits))
        hits = self.tree.mapPoint((4.999,0), 0.001)
        self.failUnless(len(hits) == 2)
        self.failUnless(hits[0][0] is self.a1 or hits[1][0] is self.a1)
        self.failUnless(hits[0][0] is self.a2 or hits[1][0] is self.a2)
        self.failUnless(hits[0][1] == (5,0))

        hits = self.tree.mapPoint(self.p2)
        self.failUnless(len(hits) == 1)
        self.failUnless(hits[0][0] is self.a3)
        self.failUnless(hits[0][1] == self.p2)

if __name__ == "__main__":
    ArcTestSuite = unittest.TestSuite()
    ArcTestSuite.addTest(ArcTestCase("testCreation"))
    ArcTestSuite.addTest(ArcTestCase("testComparison"))
    ArcTestSuite.addTest(ArcTestCase("testPointMapping"))
    ArcTestSuite.addTest(ArcTestCase("testArea"))
    ArcTestSuite.addTest(ArcTestCase("testLength"))

    ArcTreeTestSuite = unittest.TestSuite()
    ArcTreeTestSuite.addTest(ArcTreeTestCase("testStorage"))
    ArcTreeTestSuite.addTest(ArcTreeTestCase("testBinscan"))
    ArcTreeTestSuite.addTest(ArcTreeTestCase("testScan"))
    ArcTreeTestSuite.addTest(ArcTreeTestCase("testRemove"))
    ArcTreeTestSuite.addTest(ArcTreeTestCase("testMapping"))
    unittest.main()
        
