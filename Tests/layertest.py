#
# Copyright (c) 2002, 2004 Art Haas
#
#
# test layers
#

import unittest
import math
import random

import Generic.point as point
import Generic.segment as segment
import Generic.circle as circle
import Generic.arc as arc
import Generic.hcline as hcline
import Generic.vcline as vcline
import Generic.acline as acline
import Generic.cline as cline
import Generic.ccircle as ccircle
import Generic.layer as layer



def absdiff(testval, goodval):
    return abs(testval - goodval)

class LayerTestCase(unittest.TestCase):
    def setUp(self):
        self.l1 = layer.Layer('layer1')
        self.p1 = point.Point(10,0)
        self.p2 = point.Point(0,10)
        self.p3 = point.Point(-10,0)
        self.p4 = point.Point(0,-10)
        self.p5 = point.Point(0,0)
        self.s1 = segment.Segment(self.p1, self.p2)
        self.s2 = segment.Segment(self.p2, self.p3)
        self.s3 = segment.Segment(self.p3, self.p4)
        self.s4 = segment.Segment(self.p4, self.p1)
        self.c1 = circle.Circle(self.p1, 5)
        self.c2 = circle.Circle(self.p2, 5)
        self.c3 = circle.Circle(self.p3, 5)
        self.c4 = circle.Circle(self.p4, 5)
        self.a1 = arc.Arc(self.p5, 5, 315, 45 )
        self.a2 = arc.Arc(self.p5, 5, 135, 225)
        self.h1 = hcline.HCLine(self.p2)
        self.h2 = hcline.HCLine(self.p4)
        self.v1 = vcline.VCLine(self.p1)
        self.v2 = vcline.VCLine(self.p3)
        self.al1 = acline.ACLine(self.p5, 60)
        self.al2 = acline.ACLine(self.p5, -60)
        self.cl1 = cline.CLine(self.p1, self.p3)
        self.cl2 = cline.CLine(self.p2, self.p4)
        self.cl3 = cline.CLine(self.p1, self.p2)
        self.cl4 = cline.CLine(self.p2, self.p3)
        self.cl5 = cline.CLine(self.p3, self.p4)
        self.cl6 = cline.CLine(self.p4, self.p1)

    def testCreation(self):
        self.failUnless(self.l1.getName() == 'layer1')
        self.failUnless(self.l1.getParent() is None)
        self.failIf(self.l1.hasChildren())
        self.failUnless(self.l1.getVisibility())
        self.failUnless(absdiff(self.l1.getScale(),1) < 1e-10)

    def testGetSetName(self):
        self.l1.setName('newname')
        self.failUnless(self.l1.getName() == 'newname')

    def testAddDelChild(self):
        t1 = layer.Layer('child1')
        t2 = layer.Layer('child2')
        t3 = layer.Layer('child3')
        self.l1.addChild(t1)
        self.failUnless(self.l1.hasChildren())
        self.failUnless(len(self.l1.getChildren()) == 1)
        self.l1.addChild(t2)
        self.failUnless(len(self.l1.getChildren()) == 2)
        self.l1.addChild(t3)
        self.failUnless(len(self.l1.getChildren()) == 3)

        self.l1.delChild(t1)
        self.failUnless(len(self.l1.getChildren()) == 2)
        self.l1.delChild(t3)
        self.failUnless(len(self.l1.getChildren()) == 1)
        self.l1.delChild(t2)
        self.failIf(self.l1.hasChildren())

        temp = layer.Layer('temp')
        self.failUnlessRaises(ValueError, self.l1.delChild, temp)

    def testGetSetScale(self):
        old_scale = self.l1.getScale()
        self.failUnless(absdiff(old_scale, 1) < 1e-10)
        new_scale = 2
        self.l1.setScale(new_scale)
        self.failUnless(absdiff(self.l1.getScale(), new_scale) < 1e-10)
        new_scale = 0.5
        self.l1.setScale(new_scale)
        self.failUnless(absdiff(self.l1.getScale(), new_scale) < 1e-10)
        self.failUnlessRaises(ValueError, self.l1.setScale, 'asdf')
        self.failUnlessRaises(ValueError, self.l1.setScale, -1)

    def testAddDelObject(self):
        points = []
        segments = []
        circles = []
        arcs = []
        hclines = []
        vclines = []
        aclines = []
        clines = []
        ccircles = []
        seed = 100
        random.seed(seed)
        count = 1000
        for i in xrange(count):
            x = -100.0 + (random.random() * 200.0)
            y = -100.0 + (random.random() * 200.0)
            p = point.Point(x,y)
            self.l1.addObject(p)
            points.append(p)
        for i in xrange(1, count):
            pp = points[i-1]
            pi = points[i]
            seg = segment.Segment(pp, pi)
            self.l1.addObject(seg)
            segments.append(seg)
            cir = circle.Circle(pi, i)
            self.l1.addObject(cir)
            circles.append(cir)
            a = arc.Arc(pp, i, i, i+1)
            self.l1.addObject(a)
            arcs.append(a)
            hcl = hcline.HCLine(pi)
            self.l1.addObject(hcl)
            hclines.append(hcl)
            vcl = vcline.VCLine(pp)
            self.l1.addObject(vcl)
            vclines.append(vcl)
            acl = acline.ACLine(pi, 30)
            self.l1.addObject(acl)
            aclines.append(acl)
            cl = cline.CLine(pp,pi)
            self.l1.addObject(cl)
            clines.append(cl)
            cc = ccircle.CCircle(pi, i)
            self.l1.addObject(cc)
            ccircles.append(cc)
        for pt in points:
            self.failUnless(pt in self.l1)
            self.l1.delObject(pt)
            self.failUnless(pt in self.l1)
        for seg in segments:
            self.failUnless(seg in self.l1)
            self.l1.delObject(seg)
            self.failIf(seg in self.l1)
        for cir in circles:
            self.failUnless(cir in self.l1)
            self.l1.delObject(cir)
            self.failIf(cir in self.l1)
        for a in arcs:
            self.failUnless(a in self.l1)
            self.l1.delObject(a)
            self.failIf(a in self.l1)
        for hcl in hclines:
            self.failUnless(hcl in self.l1)
            self.l1.delObject(hcl)
            self.failIf(hcl in self.l1)
        for vcl in vclines:
            self.failUnless(vcl in self.l1)
            self.l1.delObject(vcl)
            self.failIf(vcl in self.l1)
        for acl in aclines:
            self.failUnless(acl in self.l1)
            self.l1.delObject(acl)
            self.failIf(acl in self.l1)
        for cl in clines:
            self.failUnless(cl in self.l1)
            self.l1.delObject(cl)
            self.failIf(cl in self.l1)
        for cc in ccircles:
            self.failUnless(cc in self.l1)
            self.l1.delObject(cc)
            self.failIf(cc in self.l1)
        for pt in points:
            self.failIf(pt in self.l1)
            
    def testHasPoint(self):
        self.l1.addObject(self.p1)
        self.l1.addObject(self.p2)
        self.l1.addObject(self.p3)
        self.l1.addObject(self.p4)
        self.l1.addObject(self.p5)
        sp = self.l1.find('point', 10, 0)
        self.failUnless(sp is self.p1)
        sp = self.l1.find('point', 0, 10)
        self.failUnless(sp is self.p2)
        sp = self.l1.find('point', -10, 0)
        self.failUnless(sp is self.p3)
        sp = self.l1.find('point', 0, -10)
        self.failUnless(sp is self.p4)
        sp = self.l1.find('point', 0, 0)
        self.failUnless(sp is self.p5)

        sp = self.l1.find('point', 0.001, 0.001)
        self.failUnless(sp is None)
        sp = self.l1.find('point', 0.001, 0.001, 0.001)
        self.failUnless(sp is self.p5)

    def testMapPoint(self):
        self.l1.addObject(self.p1)
        self.l1.addObject(self.p2)
        self.l1.addObject(self.p3)
        self.l1.addObject(self.p4)
        self.l1.addObject(self.p5)
        self.l1.addObject(self.s1)
        self.l1.addObject(self.s2)
        self.l1.addObject(self.s3)
        self.l1.addObject(self.s4)
        self.l1.addObject(self.c1)
        self.l1.addObject(self.c2)
        self.l1.addObject(self.c3)
        self.l1.addObject(self.c4)
        self.l1.addObject(self.a1)
        self.l1.addObject(self.a2)
        self.l1.addObject(self.h1)
        self.l1.addObject(self.h2)
        self.l1.addObject(self.v1)
        self.l1.addObject(self.v2)
        self.l1.addObject(self.al1)
        self.l1.addObject(self.al2)
        self.l1.addObject(self.cl1)
        self.l1.addObject(self.cl2)
        self.l1.addObject(self.cl3)
        self.l1.addObject(self.cl4)
        self.l1.addObject(self.cl5)
        self.l1.addObject(self.cl6)
        
        hits = self.l1.mapPoint((5,0))
        self.failUnless(len(hits) == 2)
        self.failUnless(hits[0][1] == (5,0))
        self.failUnless(hits[1][1] == (5,0))

        hits = self.l1.mapPoint((15,0))
        self.failUnless(len(hits) == 2)
        self.failUnless(hits[0][0] is self.c1 or hits[1][0] is self.c1)
        self.failUnless(hits[0][0] is self.cl1 or hits[1][0] is self.cl1)
        self.failUnless(hits[0][1] == (15,0))
        self.failUnless(hits[1][1] == (15,0))

        hits = self.l1.mapPoint((10,10))
        self.failUnless(len(hits) == 2)
        self.failUnless(hits[0][0] is self.h1 or hits[1][0] is self.h1)
        self.failUnless(hits[0][0] is self.v1 or hits[1][0] is self.v1)
        self.failUnless(hits[0][1] == (10,10))
        self.failUnless(hits[1][1] == (10,10))

    def testBoundary(self):
        bounds = self.l1.getBoundary()
        self.failUnless(bounds == (-1.0,-1.0,1.0,1.0))

        self.l1.addObject(self.p1)
        self.l1.addObject(self.p2)
        self.l1.addObject(self.p3)
        self.l1.addObject(self.p4)
        self.l1.addObject(self.p5)

        bounds = self.l1.getBoundary()
        self.failUnless(bounds == (-10.0,-10.0,10.0,10.0))

        self.l1.addObject(self.c1)
        self.l1.addObject(self.c2)
        self.l1.addObject(self.c3)
        self.l1.addObject(self.c4)

        bounds = self.l1.getBoundary()
        self.failUnless(bounds == (-15.0,-15.0,15.0,15.0))

        self.l1.delObject(self.c2)
        self.l1.delObject(self.c4)

        bounds = self.l1.getBoundary()
        self.failUnless(bounds == (-15.0,-5.0,15.0,5.0))

    def testObjsInRegion(self):
        self.l1.addObject(self.p1)
        self.l1.addObject(self.p2)
        self.l1.addObject(self.p3)
        self.l1.addObject(self.p4)
        self.l1.addObject(self.p5)

        objs = self.l1.objsInRegion(-10.1,-10.1,10.1,10.1)
        self.failUnless(len(objs) == 5)

        objs = self.l1.objsInRegion(-5,-5,5,5)
        self.failUnless(len(objs) == 1)

        self.l1.addObject(self.s1)
        self.l1.addObject(self.s2)
        self.l1.addObject(self.s3)
        self.l1.addObject(self.s4)
        
        objs = self.l1.objsInRegion(-6,-6,6,6)
        self.failUnless(len(objs) == 5)

        objs = self.l1.objsInRegion(-6,-6,6,6, True)
        self.failUnless(len(objs) == 1)

        objs = self.l1.objsInRegion(4,4,6,6)
        self.failUnless(len(objs) == 1)
        self.failUnless(objs[0] is self.s1)

        objs = self.l1.objsInRegion(4,4,6,6, True)
        self.failIf(len(objs))
        
        objs = self.l1.objsInRegion(9,-1,11,1)
        self.failUnless(len(objs) == 3)
        self.failUnless(objs[0] is self.p1 or \
                        objs[0] is self.s1 or \
                        objs[0] is self.s4)
        self.failUnless(objs[1] is self.p1 or \
                        objs[1] is self.s1 or \
                        objs[1] is self.s4)
        self.failUnless(objs[2] is self.p1 or \
                        objs[2] is self.s1 or \
                        objs[2] is self.s4)

        objs = self.l1.objsInRegion(9,-1,11,1, True)
        self.failUnless(len(objs) == 1)
        self.failUnless(objs[0] is self.p1)

        objs = self.l1.objsInRegion(4,-6,6,-4)
        self.failUnless(len(objs) == 1)
        self.failUnless(objs[0] is self.s4)

        objs = self.l1.objsInRegion(4,-6,6,-4, True)
        self.failIf(len(objs))

        objs = self.l1.objsInRegion(-1,-11,1,-9)
        self.failUnless(len(objs) == 3)
        self.failUnless(objs[0] is self.p4 or \
                        objs[0] is self.s3 or \
                        objs[0] is self.s4)
        self.failUnless(objs[1] is self.p4 or \
                        objs[1] is self.s3 or \
                        objs[1] is self.s4)
        self.failUnless(objs[2] is self.p4 or \
                        objs[2] is self.s3 or \
                        objs[2] is self.s4)

        objs = self.l1.objsInRegion(-1,-11,1,-9, True)
        self.failUnless(len(objs) == 1)
        self.failUnless(objs[0] is self.p4)

        objs = self.l1.objsInRegion(-6,-6,-4,-4)
        self.failUnless(len(objs) == 1)
        self.failUnless(objs[0] is self.s3)

        objs = self.l1.objsInRegion(-6,-6,-4,-4, True)
        self.failIf(len(objs))

        objs = self.l1.objsInRegion(-11,-1,-9,1)
        self.failUnless(len(objs) == 3)
        self.failUnless(objs[0] is self.p3 or \
                        objs[0] is self.s2 or \
                        objs[0] is self.s3)
        self.failUnless(objs[1] is self.p3 or \
                        objs[1] is self.s2 or \
                        objs[1] is self.s3)
        self.failUnless(objs[2] is self.p3 or \
                        objs[2] is self.s2 or \
                        objs[2] is self.s3)

        objs = self.l1.objsInRegion(-11,-1,-9,1, True)
        self.failUnless(len(objs) == 1)
        self.failUnless(objs[0] is self.p3)

        objs = self.l1.objsInRegion(-6,4,-4,6)
        self.failUnless(len(objs) == 1)
        self.failUnless(objs[0] is self.s2)

        objs = self.l1.objsInRegion(-6,4,-4,6, True)
        self.failIf(len(objs))

        objs = self.l1.objsInRegion(-1,9,1,11)
        self.failUnless(len(objs) == 3)
        self.failUnless(objs[0] is self.p2 or \
                        objs[0] is self.s1 or \
                        objs[0] is self.s2)
        self.failUnless(objs[1] is self.p2 or \
                        objs[1] is self.s1 or \
                        objs[1] is self.s2)
        self.failUnless(objs[2] is self.p2 or \
                        objs[2] is self.s1 or \
                        objs[2] is self.s2)

        objs = self.l1.objsInRegion(-1,9,1,11, True)
        self.failUnless(len(objs) == 1)
        self.failUnless(objs[0] is self.p2)

        #
        # add circles and get rid of lines
        #
        
        self.l1.addObject(self.c1)
        self.l1.addObject(self.c2)
        self.l1.addObject(self.c3)
        self.l1.addObject(self.c4)
        self.l1.delObject(self.s1)
        self.l1.delObject(self.s2)
        self.l1.delObject(self.s3)
        self.l1.delObject(self.s4)
        
        objs = self.l1.objsInRegion(4,-6,16,6)
        self.failUnless(len(objs) == 2)
        self.failUnless(objs[0] is self.p1 or \
                        objs[0] is self.c1)
        self.failUnless(objs[1] is self.p1 or \
                        objs[1] is self.c1)

        objs = self.l1.objsInRegion(4,-1,6,1)
        self.failUnless(len(objs) == 1)
        self.failUnless(objs[0] is self.c1)

        objs = self.l1.objsInRegion(4,9,6,11)
        self.failUnless(len(objs) == 1)
        self.failUnless(objs[0] is self.c2)

        objs = self.l1.objsInRegion(-11,-6,-9,-4)
        self.failUnless(len(objs) == 1)
        self.failUnless(objs[0] is self.c3)

        objs = self.l1.objsInRegion(-1,-6, 1,-4)
        self.failUnless(len(objs) == 1)
        self.failUnless(objs[0] is self.c4)

        #
        # add horizontal and vertical construction lines
        # get rid of circles
        #
        
        self.l1.addObject(self.h1)
        self.l1.addObject(self.h2)
        self.l1.addObject(self.v1)
        self.l1.addObject(self.v2)
        self.l1.delObject(self.c1)
        self.l1.delObject(self.c2)
        self.l1.delObject(self.c3)
        self.l1.delObject(self.c4)

        objs = self.l1.objsInRegion(20,9,21,11)
        self.failUnless(len(objs) == 1)
        self.failUnless(objs[0] is self.h1)

        objs = self.l1.objsInRegion(-20,-11,-19,-9)
        self.failUnless(len(objs) == 1)
        self.failUnless(objs[0] is self.h2)

        objs = self.l1.objsInRegion(9,20,11,22)
        self.failUnless(len(objs) == 1)
        self.failUnless(objs[0] is self.v1)

        objs = self.l1.objsInRegion(-11,-22,-9,-20)
        self.failUnless(len(objs) == 1)
        self.failUnless(objs[0] is self.v2)

        #
        # add angled construction lines
        #
        
        self.l1.addObject(self.al1)
        self.l1.addObject(self.al2)

        objs = self.l1.objsInRegion(1,0.5,4,9)
        self.failUnless(len(objs) == 1)
        self.failUnless(objs[0] is self.al1)

        objs = self.l1.objsInRegion(-4,0.5,-1, 9)
        self.failUnless(len(objs) == 1)
        self.failUnless(objs[0] is self.al2)

        #
        # add two-point construction lines
        #

        self.l1.addObject(self.cl1)
        self.l1.addObject(self.cl2)
        self.l1.addObject(self.cl3)
        self.l1.addObject(self.cl4)
        self.l1.addObject(self.cl5)
        self.l1.addObject(self.cl6)

        objs= self.l1.objsInRegion(30,-1,31,1)
        self.failUnless(len(objs) == 1)
        self.failUnless(objs[0] is self.cl1)

        objs = self.l1.objsInRegion(-1,30,1,31)
        self.failUnless(len(objs) == 1)
        self.failUnless(objs[0] is self.cl2)

        objs = self.l1.objsInRegion(11,-2.5,12,-1.5)
        self.failUnless(len(objs) == 1)
        self.failUnless(objs[0] is self.cl3)

        objs = self.l1.objsInRegion(1.5, 12, 2.5, 12.5)
        self.failUnless(len(objs) == 1)
        self.failUnless(objs[0] is self.cl4)

        objs = self.l1.objsInRegion(-12, 1.5, -11, 2.5)
        self.failUnless(len(objs) == 1)
        self.failUnless(objs[0] is self.cl5)

        objs = self.l1.objsInRegion(11,1.5,12,2.5)
        self.failUnless(len(objs) == 1)
        self.failUnless(objs[0] is self.cl6)

if __name__ == "__main__":
    LayerTestSuite = unittest.TestSuite()
    LayerTestSuite.addTest(LayerTestCase("testCreation"))
    LayerTestSuite.addTest(LayerTestCase("testGetSetName"))
    LayerTestSuite.addTest(LayerTestCase("testAddDelChild"))
    LayerTestSuite.addTest(LayerTestCase("testGetSetScale"))
    LayerTestSuite.addTest(LayerTestCase("testAddDelObject"))
    LayerTestSuite.addTest(LayerTestCase("testHasPoint"))
    LayerTestSuite.addTest(LayerTestCase("testMapPoint"))
    LayerTestSuite.addTest(LayerTestCase("testBoundary"))
    LayerTestSuite.addTest(LayerTestCase("testObjsInRegion"))

    unittest.main()
