#
# Copyright (c) 2002, Art Haas
#
#
# intersection tests
#

import unittest
import math

import Generic.point as point
import Generic.segment as segment
import Generic.circle as circle
import Generic.arc as arc
import Generic.hcline as hcline
import Generic.vcline as vcline
import Generic.acline as acline
import Generic.cline as cline
import Generic.intersections as intersections

class SegmentTestCase(unittest.TestCase):
    def setUp(self):
        self.p1 = point.Point(0,0)
        self.p2 = point.Point(10,0)
        self.p3 = point.Point(0,10)
        self.p4 = point.Point(0,-10)
        self.p5 = point.Point(10,10)
        self.p6 = point.Point(10,-10)
        self.s1 = segment.Segment(self.p1,self.p2)
        self.s2 = segment.Segment(self.p1,self.p3)
        self.s3 = segment.Segment(self.p1,self.p4)
        self.s4 = segment.Segment(self.p3,self.p6)
        self.s5 = segment.Segment(self.p4,self.p5)
        self.s6 = segment.Segment(self.p2,self.p5)
        self.s7 = segment.Segment(self.p2,self.p6)

    def test_seg_seg(self):
        ints = intersections.findIntersections(self.s1,self.s2)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (0,0))

        ints = intersections.findIntersections(self.s2,self.s3)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (0,0))

        ints = intersections.findIntersections(self.s4,self.s5)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (5,0))

        ints = intersections.findIntersections(self.s2,self.s6)
        self.failIf(len(ints))

        ints = intersections.findIntersections(self.s6,self.s7)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (10,0))

        ints = intersections.findIntersections(self.s4,self.s7)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (10,-10))

        ints = intersections.findIntersections(self.s5,self.s6)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (10,10))

    def test_seg_circle(self):
        circ = circle.Circle((5,0),2.5)
        ints = intersections.findIntersections(self.s1, circ)
        self.failUnless(len(ints) == 2)
        self.failUnless(ints[0] == (2.5,0) or ints[0] == (7.5,0))
        self.failUnless(ints[1] == (2.5,0) or ints[1] == (7.5,0))
        ints = intersections.findIntersections(circ, self.s1)
        self.failUnless(len(ints) == 2)
        self.failUnless(ints[0] == (2.5,0) or ints[0] == (7.5,0))
        self.failUnless(ints[1] == (2.5,0) or ints[1] == (7.5,0))

        circ = circle.Circle((5,2.5),2.5)
        ints = intersections.findIntersections(self.s1, circ)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (5,0))
        ints = intersections.findIntersections(circ, self.s1)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (5,0))

        circ = circle.Circle((5,3),2.5)
        ints = intersections.findIntersections(self.s1, circ)
        self.failIf(len(ints))
        ints = intersections.findIntersections(circ, self.s1)
        self.failIf(len(ints))

        circ = circle.Circle((5,0),5)
        ints = intersections.findIntersections(self.s6, circ)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (10,0))
        ints = intersections.findIntersections(circ, self.s6)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (10,0))

        ints = intersections.findIntersections(circ,self.s7)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (10,0))
        ints = intersections.findIntersections(self.s7, circ)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (10,0))

    def test_seg_hcline(self):
        hcl = hcline.HCLine((-5,5))
        ints = intersections.findIntersections(self.s2, hcl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (0,5))
        ints = intersections.findIntersections(hcl, self.s2)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (0,5))

        ints = intersections.findIntersections(self.s5, hcl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (7.5,5))
        ints = intersections.findIntersections(hcl, self.s5)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (7.5,5))

        ints = intersections.findIntersections(self.s1, hcl)
        self.failIf(len(ints))
        ints = intersections.findIntersections(hcl,self.s1)
        self.failIf(len(ints))

    def test_seg_vcline(self):
        vcl = vcline.VCLine((2.5,-2.5))

        ints = intersections.findIntersections(self.s1, vcl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (2.5,0))
        ints = intersections.findIntersections(vcl, self.s1)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (2.5,0))

        ints = intersections.findIntersections(self.s4, vcl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (2.5,5))
        ints = intersections.findIntersections(vcl, self.s4)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (2.5,5))

        ints = intersections.findIntersections(self.s2, vcl)
        self.failIf(len(ints))
        ints = intersections.findIntersections(vcl, self.s2)
        self.failIf(len(ints))

    def test_seg_acline(self):
        acl = acline.ACLine((5,0),45)
        
        ints = intersections.findIntersections(self.s1, acl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (5,0))
        ints = intersections.findIntersections(acl, self.s1)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (5,0))

        ints = intersections.findIntersections(self.s6, acl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (10,5))
        ints = intersections.findIntersections(acl, self.s6)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (10,5))

        ints = intersections.findIntersections(self.s4, acl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (5,0))
        ints = intersections.findIntersections(acl, self.s4)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (5,0))

    def test_seg_cline(self):
        cl = cline.CLine((-5,5),(15,5))
        ints = intersections.findIntersections(self.s1, cl)
        self.failIf(len(ints))
        ints = intersections.findIntersections(cl, self.s1)
        self.failIf(len(ints))

        ints = intersections.findIntersections(self.s2, cl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (0,5))
        ints = intersections.findIntersections(cl, self.s2)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (0,5))

        ints = intersections.findIntersections(self.s4, cl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (2.5,5))
        ints = intersections.findIntersections(cl, self.s4)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (2.5,5))

        cl = cline.CLine((0,-10),(10,10))
        ints = intersections.findIntersections(self.s6, cl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (10,10))
        ints = intersections.findIntersections(cl, self.s6)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (10,10))

        ints = intersections.findIntersections(self.s5, cl)
        self.failIf(len(ints))
        ints = intersections.findIntersections(cl, self.s5)
        self.failIf(len(ints))

class CircleTestCase(unittest.TestCase):
    def setUp(self):
        self.p1 = point.Point(0,0)
        self.p2 = point.Point(10,0)
        self.p3 = point.Point(0,10)
        self.p4 = point.Point(0,-10)
        self.c1 = circle.Circle(self.p1, 5)
        self.c2 = circle.Circle(self.p2, 5)
        self.c3 = circle.Circle(self.p3, 5)
        self.c4 = circle.Circle(self.p4, 5)

    def test_circ_circ(self):
        ints = intersections.findIntersections(self.c1, self.c2)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (5,0))
        
        ints = intersections.findIntersections(self.c1, self.c3)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (0,5))

        ints = intersections.findIntersections(self.c1, self.c4)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (0,-5))


        ints = intersections.findIntersections(self.c2, self.c3)
        self.failIf(len(ints))

        ints = intersections.findIntersections(self.c2, self.c4)
        self.failIf(len(ints))

        ints = intersections.findIntersections(self.c3, self.c4)
        self.failIf(len(ints))

        c1 = circle.Circle(self.p1, 10)
        c2 = circle.Circle(self.p2, 10)

        ints = intersections.findIntersections(c1, c2)
        self.failUnless(len(ints) == 2)
        dist = math.sin(math.acos(0.5)) * 10.0
        self.failUnless(ints[0] == (5,dist) or ints[0] == (5,-dist))
        self.failUnless(ints[1] == (5,dist) or ints[1] == (5,-dist))

    def test_circ_hcl(self):
        hcl = hcline.HCLine((-20,5))

        ints = intersections.findIntersections(self.c1,hcl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (0,5))
        ints = intersections.findIntersections(hcl,self.c1)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (0,5))

        ints = intersections.findIntersections(self.c2,hcl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (10,5))
        ints = intersections.findIntersections(hcl,self.c2)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (10,5))

        ints = intersections.findIntersections(self.c3,hcl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (0,5))
        ints = intersections.findIntersections(hcl,self.c3)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (0,5))

        ints = intersections.findIntersections(self.c4,hcl)
        self.failIf(len(ints))
        ints = intersections.findIntersections(hcl,self.c4)
        self.failIf(len(ints))

        hcl = hcline.HCLine((-10,0))

        ints = intersections.findIntersections(self.c1,hcl)
        self.failUnless(len(ints) == 2)
        self.failUnless(ints[0] == (-5,0) or ints[0] == (5,0))
        self.failUnless(ints[1] == (-5,0) or ints[1] == (5,0))
        ints = intersections.findIntersections(hcl,self.c1)
        self.failUnless(len(ints) == 2)
        self.failUnless(ints[0] == (-5,0) or ints[0] == (5,0))
        self.failUnless(ints[1] == (-5,0) or ints[1] == (5,0))

    def test_circ_vcl(self):
        vcl = vcline.VCLine((5,-20))

        ints = intersections.findIntersections(self.c1,vcl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (5,0))
        ints = intersections.findIntersections(vcl, self.c1)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (5,0))

        ints = intersections.findIntersections(self.c2,vcl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (5,0))
        ints = intersections.findIntersections(vcl,self.c2)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (5,0))

        ints = intersections.findIntersections(self.c3,vcl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (5,10))
        ints = intersections.findIntersections(vcl,self.c3)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (5,10))

        ints = intersections.findIntersections(self.c4,vcl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (5,-10))
        ints = intersections.findIntersections(vcl,self.c4)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (5,-10))

        vcl = vcline.VCLine((10,-30))

        ints = intersections.findIntersections(self.c2,vcl)
        self.failUnless(len(ints) == 2)
        self.failUnless(ints[0] == (10,-5) or ints[0] == (10,5))
        self.failUnless(ints[1] == (10,-5) or ints[1] == (10,5))
        ints = intersections.findIntersections(vcl,self.c2)
        self.failUnless(len(ints) == 2)
        self.failUnless(ints[0] == (10,-5) or ints[0] == (10,5))
        self.failUnless(ints[1] == (10,-5) or ints[1] == (10,5))

    def test_circ_acl(self):
        acl = acline.ACLine((-20,-20), 45)

        dist = 5.0/math.sqrt(2.0)
        ints = intersections.findIntersections(self.c1,acl)
        self.failUnless(len(ints) == 2)
        self.failUnless(ints[0] == (-dist,-dist) or ints[0] == (dist,dist))
        self.failUnless(ints[1] == (-dist,-dist) or ints[1] == (dist,dist))
        ints = intersections.findIntersections(acl,self.c1)
        self.failUnless(len(ints) == 2)
        self.failUnless(ints[0] == (-dist,-dist) or ints[0] == (dist,dist))
        self.failUnless(ints[1] == (-dist,-dist) or ints[1] == (dist,dist))

        ints = intersections.findIntersections(self.c2,acl)
        self.failIf(len(ints))
        ints = intersections.findIntersections(acl,self.c2)
        self.failIf(len(ints))

        ints = intersections.findIntersections(self.c3,acl)
        self.failIf(len(ints))
        ints = intersections.findIntersections(acl,self.c3)
        self.failIf(len(ints))

        ints = intersections.findIntersections(self.c4,acl)
        self.failIf(len(ints))
        ints = intersections.findIntersections(acl,self.c4)
        self.failIf(len(ints))

        acl = acline.ACLine((-10,-5),0)

        ints = intersections.findIntersections(self.c1,acl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (0,-5))
        ints = intersections.findIntersections(acl,self.c1)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (0,-5))

        ints = intersections.findIntersections(self.c2,acl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (10,-5))
        ints = intersections.findIntersections(acl,self.c2)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (10,-5))
        
        ints = intersections.findIntersections(self.c3,acl)
        self.failIf(len(ints))
        ints = intersections.findIntersections(acl,self.c3)
        self.failIf(len(ints))

        ints = intersections.findIntersections(self.c4,acl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (0,-5))
        ints = intersections.findIntersections(acl,self.c4)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (0,-5))

    def test_circ_cline(self):
        dist = 5.0/math.sqrt(2.0)
        cl = cline.CLine((0,2*dist),(dist,dist))
        
        ints = intersections.findIntersections(self.c1,cl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (dist,dist))
        ints = intersections.findIntersections(cl,self.c1)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (dist,dist))

        cl = cline.CLine((-20,10),(-18,10))
        ints = intersections.findIntersections(self.c3,cl)
        self.failUnless(len(ints) == 2)
        self.failUnless(ints[0] == (-5,10) or ints[0] == (5,10))
        self.failUnless(ints[1] == (-5,10) or ints[1] == (5,10))
        ints = intersections.findIntersections(cl,self.c3)
        self.failUnless(len(ints) == 2)
        self.failUnless(ints[0] == (-5,10) or ints[0] == (5,10))
        self.failUnless(ints[1] == (-5,10) or ints[1] == (5,10))

        ints = intersections.findIntersections(self.c4, cl)
        self.failIf(len(ints))
        ints = intersections.findIntersections(cl,self.c4)
        self.failIf(len(ints))
    

class HCLineTestCase(unittest.TestCase):
    def setUp(self):
        self.p1 = point.Point(0,0)
        self.p2 = point.Point(0,10)
        self.p3 = point.Point(0,-10)
        self.h1 = hcline.HCLine(self.p1)
        self.h2 = hcline.HCLine(self.p2)
        self.h3 = hcline.HCLine(self.p3)

    def test_hcl_hcl(self):

        ints = intersections.findIntersections(self.h1,self.h2)
        self.failIf(len(ints))

        ints = intersections.findIntersections(self.h1,self.h3)
        self.failIf(len(ints))

        ints = intersections.findIntersections(self.h2,self.h3)
        self.failIf(len(ints))

    def test_hcl_vcl(self):
        vcl = vcline.VCLine((4,4))
        
        ints = intersections.findIntersections(self.h1,vcl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (4,0))
        ints = intersections.findIntersections(vcl,self.h1)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (4,0))

        ints = intersections.findIntersections(self.h2,vcl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (4,10))
        ints = intersections.findIntersections(vcl,self.h2)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (4,10))

        ints = intersections.findIntersections(self.h3,vcl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (4,-10))
        ints = intersections.findIntersections(vcl,self.h3)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (4,-10))

    def test_hcl_acl(self):
        acl = acline.ACLine((10,0),45)

        ints = intersections.findIntersections(self.h1,acl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (10,0))
        ints = intersections.findIntersections(acl,self.h1)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (10,0))

        ints = intersections.findIntersections(self.h2,acl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (20,10))
        ints = intersections.findIntersections(acl,self.h2)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (20,10))

        ints = intersections.findIntersections(self.h3,acl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (0,-10))
        ints = intersections.findIntersections(acl,self.h3)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (0,-10))

        acl = acline.ACLine((0,5),0)

        ints = intersections.findIntersections(self.h1,acl)
        self.failIf(len(ints))
        ints = intersections.findIntersections(acl,self.h1)
        self.failIf(len(ints))

        ints = intersections.findIntersections(self.h1,acl)
        self.failIf(len(ints))
        ints = intersections.findIntersections(acl,self.h1)
        self.failIf(len(ints))

        ints = intersections.findIntersections(self.h2,acl)
        self.failIf(len(ints))
        ints = intersections.findIntersections(acl,self.h2)
        self.failIf(len(ints))

    def test_hcl_cl(self):
        cl = cline.CLine((10,0),(5,-5))

        ints = intersections.findIntersections(self.h1,cl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (10,0))
        ints = intersections.findIntersections(cl,self.h1)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (10,0))

        ints = intersections.findIntersections(self.h2,cl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (20,10))
        ints = intersections.findIntersections(cl,self.h2)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (20,10))

        ints = intersections.findIntersections(self.h3,cl)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (0,-10))
        ints = intersections.findIntersections(cl,self.h3)
        self.failUnless(len(ints) == 1)
        self.failUnless(ints[0] == (0,-10))

        cl = cline.CLine((0,5),(3,5))

        ints = intersections.findIntersections(self.h1,cl)
        self.failIf(len(ints))
        ints = intersections.findIntersections(cl,self.h1)
        self.failIf(len(ints))

        ints = intersections.findIntersections(self.h2,cl)
        self.failIf(len(ints))
        ints = intersections.findIntersections(cl,self.h2)
        self.failIf(len(ints))

        ints = intersections.findIntersections(self.h3,cl)
        self.failIf(len(ints))
        ints = intersections.findIntersections(cl,self.h3)
        self.failIf(len(ints))

if __name__ == "__main__":
    SegmentTestSuite = unittest.TestSuite()
    SegmentTestSuite.addTest(SegmentTestCase("test_seg_seg"))
    SegmentTestSuite.addTest(SegmentTestCase("test_seg_circle"))
    SegmentTestSuite.addTest(SegmentTestCase("test_seg_hcline"))
    SegmentTestSuite.addTest(SegmentTestCase("test_seg_vcline"))
    SegmentTestSuite.addTest(SegmentTestCase("test_seg_acline"))
    SegmentTestSuite.addTest(SegmentTestCase("test_seg_cline"))
    
    CircleTestSuite = unittest.TestSuite()
    CircleTestSuite.addTest(CircleTestCase("test_circ_circ"))
    CircleTestSuite.addTest(CircleTestCase("test_circ_hcl"))
    CircleTestSuite.addTest(CircleTestCase("test_circ_vcl"))
    CircleTestSuite.addTest(CircleTestCase("test_circ_acl"))
    CircleTestSuite.addTest(CircleTestCase("test_circ_cline"))

    HCLineTestSuite = unittest.TestSuite()
    HCLineTestSuite.addTest(HCLineTestCase("test_hcl_hcl"))
    HCLineTestSuite.addTest(HCLineTestCase("test_hcl_vcl"))
    HCLineTestSuite.addTest(HCLineTestCase("test_hcl_acl"))
    HCLineTestSuite.addTest(HCLineTestCase("test_hcl_cl"))
    
    unittest.main()
