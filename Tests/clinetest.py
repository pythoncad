#
# Copyright (c) 2002, Art Haas
#
#
# test two-point construction lines
#

import unittest
import math
import random

import Generic.point as point
import Generic.cline as cline

def absdiff(testval, goodval):
    return abs(testval - goodval)

class CLineTestCase(unittest.TestCase):
    def setUp(self):
        self.p1 = point.Point(10,0)
        self.p2 = point.Point(0,5)
        self.p3 = point.Point(-10,0)
        self.p4 = point.Point(0,-5)
        self.c1 = cline.CLine(self.p1, self.p3)
        self.c2 = cline.CLine(self.p2, self.p4)
        self.c3 = cline.CLine(self.p1, self.p2)
        self.c4 = cline.CLine(self.p2, self.p3)
        self.c5 = cline.CLine(self.p3, self.p4)
        self.c6 = cline.CLine(self.p4, self.p1)

    def testCreation(self):
        p1, p2 = self.c1.getKeypoints()
        self.failUnless(p1 is self.p1 and p2 is self.p3)
        p1, p2 = self.c2.getKeypoints()
        self.failUnless(p1 is self.p2 and p2 is self.p4)
        p1, p2 = self.c3.getKeypoints()
        self.failUnless(p1 is self.p1 and p2 is self.p2)
        p1, p2 = self.c4.getKeypoints()
        self.failUnless(p1 is self.p2 and p2 is self.p3)
        p1, p2 = self.c5.getKeypoints()
        self.failUnless(p1 is self.p3 and p2 is self.p4)
        p1, p2 = self.c6.getKeypoints()
        self.failUnless(p1 is self.p4 and p2 is self.p1)

    def testComparison(self):
        self.failUnless(self.c1 == self.c1)
        self.failUnless(self.c1 < self.c2 and self.c2 > self.c1)
        self.failUnless(self.c1 > self.c3 and self.c3 < self.c1)
        self.failUnless(self.c1 < self.c4 and self.c4 > self.c1)
        self.failUnless(self.c1 > self.c5 and self.c5 < self.c1)
        self.failUnless(self.c1 < self.c6 and self.c6 > self.c1)

        self.failUnless(self.c2 == self.c2)
        self.failUnless(self.c2 > self.c3 and self.c3 < self.c2)
        self.failUnless(self.c2 > self.c4 and self.c4 < self.c2)
        self.failUnless(self.c2 > self.c5 and self.c5 < self.c2)
        self.failUnless(self.c2 > self.c6 and self.c6 < self.c2)

        self.failUnless(self.c3 == self.c3)
        self.failUnless(self.c3 < self.c4 and self.c4 > self.c3)
        self.failUnless(self.c3 > self.c5 and self.c5 < self.c3)
        self.failUnless(self.c3 < self.c6 and self.c6 > self.c3)

        self.failUnless(self.c4 == self.c4)
        self.failUnless(self.c4 > self.c5 and self.c5 < self.c4)
        self.failUnless(self.c4 > self.c6 and self.c6 < self.c4)

        self.failUnless(self.c5 == self.c5)
        self.failUnless(self.c5 < self.c6 and self.c6 > self.c5)

        self.failUnless(self.c6 == self.c6)

        temp = cline.CLine(self.p3, self.p1)
        self.failUnless(temp == self.c1 and self.c1 == temp)

        temp = cline.CLine(self.p4, self.p2)
        self.failUnless(temp == self.c2 and self.c2 == temp)

    def testPointMapping(self):
        p = self.c1.mapPoint((0,0))
        self.failUnless(p == (0,0))
        p = self.c1.mapPoint((0,0.001))
        self.failUnless(p is None)
        p = self.c1.mapPoint((0,0.001), 0.001)
        self.failUnless(p == (0,0))
        p = self.c1.mapPoint((0,-0.001))
        self.failUnless(p is None)
        p = self.c1.mapPoint((0,-0.001), 0.001)
        self.failUnless(p == (0,0))

        p = self.c2.mapPoint((0,0))
        self.failUnless(p == (0,0))
        p = self.c2.mapPoint((0.001,0))
        self.failUnless(p is None)
        p = self.c2.mapPoint((0.001,0), 0.001)
        self.failUnless(p == (0,0))
        p = self.c2.mapPoint((-0.001,0))
        self.failUnless(p is None)
        p = self.c2.mapPoint((-0.001,0), 0.001)
        self.failUnless(p == (0,0))

        p = self.c3.mapPoint((5,2.5))
        self.failUnless(p == (5,2.5))
        p = self.c3.mapPoint((5,2.501))
        self.failUnless(p is None)
        p = self.c3.mapPoint((5,2.501), 0.001)
        self.failUnless(p == (4.9996, 2.5002))
        p = self.c3.mapPoint((5,2.499))
        self.failUnless(p is None)
        p = self.c3.mapPoint((5,2.499), 0.001)
        self.failUnless(p == (5.0004, 2.4998))

class CLineTreeTestCase(unittest.TestCase):
    def setUp(self):
        self.tree = cline.CLineTree()
        self.p1 = point.Point(10,0)
        self.p2 = point.Point(0,5)
        self.p3 = point.Point(-10,0)
        self.p4 = point.Point(0,-5)
        self.c1 = cline.CLine(self.p1, self.p3)
        self.c2 = cline.CLine(self.p2, self.p4)
        self.c3 = cline.CLine(self.p1, self.p2)
        self.c4 = cline.CLine(self.p2, self.p3)
        self.c5 = cline.CLine(self.p3, self.p4)
        self.c6 = cline.CLine(self.p4, self.p1)

    def testStorage(self):
        self.tree.store(self.c1)
        self.failUnless(len(self.tree) == 1)
        self.failUnless(self.c1 in self.tree)
        self.tree.store(self.c2)
        self.failUnless(len(self.tree) == 2)
        self.failUnless(self.c2 in self.tree)
        self.tree.store(self.c3)
        self.failUnless(len(self.tree) == 3)
        self.failUnless(self.c3 in self.tree)
        self.tree.store(self.c4)
        self.failUnless(len(self.tree) == 4)
        self.failUnless(self.c4 in self.tree)
        self.tree.store(self.c5)
        self.failUnless(len(self.tree) == 5)
        self.failUnless(self.c5 in self.tree)
        self.tree.store(self.c6)
        self.failUnless(len(self.tree) == 6)
        self.failUnless(self.c6 in self.tree)

        temp = cline.CLine(self.p3, self.p1)
        self.failUnless(temp in self.tree)

        seed = 100
        random.seed(seed)
        for i in xrange(1000):
            x = -100.0 + (random.random() * 200.0)
            y = -100.0 + (random.random() * 200.0)
            p1 = point.Point(x,y)
            x = -100.0 + (random.random() * 200.0)
            y = -100.0 + (random.random() * 200.0)
            p2 = point.Point(x,y)
            c = cline.CLine(p1, p2)
            self.tree.store(c)
        self.validateTree()


    def validateTree(self):
        for i in xrange(1, len(self.tree)):
            cp = self.tree[i-1]
            ci = self.tree[i]
            cp_p1, cp_p2 = cp.getKeypoints()
            cp_vert = False
            if abs(cp_p2.x - cp_p1.x) < 1e-6: # close enough ...
                cp_vert = True
            ci_p1, ci_p2 = ci.getKeypoints()                
            ci_vert = False
            if abs(ci_p2.x - ci_p1.x) < 1e-6: # close enough ...
                ci_vert = True
            self.failIf(cp_vert and not ci_vert)
            if cp_vert and ci_vert:
                self.failUnless(cp_p1.x <= ci_p1.x)
            else:
                if not ci_vert:
                    cp_m = (cp_p2.y - cp_p1.y)/(cp_p2.x - cp_p1.x)
                    cp_b = cp_p2.y - (cp_m * cp_p2.x)
                    ci_m = (ci_p2.y - ci_p1.y)/(ci_p2.x - ci_p1.x)
                    ci_b = ci_p2.y - (ci_m * ci_p2.x)
                    self.failUnless(cp_m <= ci_m)
                    if abs(cp_m - ci_m) < 1e-10:
                        self.failUnless(cp_b <= ci_b)
            
            
    def testBinscan(self):
        self.tree.store(self.c1)
        self.tree.store(self.c2)
        self.tree.store(self.c3)
        self.tree.store(self.c4)
        self.tree.store(self.c5)
        self.tree.store(self.c6)
        sc = self.tree.binscan(self.c1)
        self.failUnless(sc is self.c1)
        sc = self.tree.binscan(self.c2)
        self.failUnless(sc is self.c2)
        sc = self.tree.binscan(self.c3)
        self.failUnless(sc is self.c3)
        sc = self.tree.binscan(self.c4)
        self.failUnless(sc is self.c4)
        sc = self.tree.binscan(self.c5)
        self.failUnless(sc is self.c5)
        sc = self.tree.binscan(self.c6)
        self.failUnless(sc is self.c6)
        
        temp = cline.CLine(self.p3, self.p1)
        sc = self.tree.binscan(temp)
        self.failUnless(sc is self.c1)

    def testScan(self):
        self.tree.store(self.c1)
        self.tree.store(self.c2)
        self.tree.store(self.c3)
        self.tree.store(self.c4)
        self.tree.store(self.c5)
        self.tree.store(self.c6)
        sc = self.tree.scan(self.c1)
        self.failUnless(sc is self.c1)
        sc = self.tree.scan(self.c2)
        self.failUnless(sc is self.c2)
        sc = self.tree.scan(self.c3)
        self.failUnless(sc is self.c3)
        sc = self.tree.scan(self.c4)
        self.failUnless(sc is self.c4)
        sc = self.tree.scan(self.c5)
        self.failUnless(sc is self.c5)
        sc = self.tree.scan(self.c6)
        self.failUnless(sc is self.c6)
        
        temp = cline.CLine(self.p3, self.p1)
        sc = self.tree.scan(temp)
        self.failUnless(sc is self.c1)

    def testRemove(self):
        self.tree.store(self.c1)
        self.tree.store(self.c2)
        self.tree.store(self.c3)
        self.tree.store(self.c4)
        self.tree.store(self.c5)
        self.tree.store(self.c6)
        self.tree.remove(self.c1)
        self.failUnless(len(self.tree) == 5)
        self.failIf(self.c1 in self.tree)
        self.tree.remove(self.c2)
        self.failUnless(len(self.tree) == 4)
        self.failIf(self.c2 in self.tree)
        self.tree.remove(self.c3)
        self.failUnless(len(self.tree) == 3)
        self.failIf(self.c3 in self.tree)
        self.tree.remove(self.c4)
        self.failUnless(len(self.tree) == 2)
        self.failIf(self.c4 in self.tree)
        self.tree.remove(self.c5)
        self.failUnless(len(self.tree) == 1)
        self.failIf(self.c5 in self.tree)
        self.tree.remove(self.c6)
        self.failIf(len(self.tree))
        self.failIf(self.c6 in self.tree)

    def testPointMapping(self):
        self.tree.store(self.c1)
        self.tree.store(self.c2)
        self.tree.store(self.c3)
        self.tree.store(self.c4)
        self.tree.store(self.c5)
        self.tree.store(self.c6)

        hits = self.tree.mapPoint((0,0))
        self.failUnless(len(hits) == 2)
        self.failUnless(hits[0][0] is self.c1 or hits[1][0] is self.c1)
        self.failUnless(hits[0][0] is self.c2 or hits[1][0] is self.c2)
        self.failUnless(hits[0][1] == (0,0))
        self.failUnless(hits[1][1] == (0,0))

        hits = self.tree.mapPoint((5,0))
        self.failUnless(len(hits) == 1)
        self.failUnless(hits[0][0] is self.c1)
        self.failUnless(hits[0][1] == (5,0))
        
        hits = self.tree.mapPoint((5,0.001))
        self.failIf(len(hits))
        hits = self.tree.mapPoint((5,0.001), 0.001)
        self.failUnless(len(hits) == 1)
        self.failUnless(hits[0][0] is self.c1)
        self.failUnless(hits[0][1] == (5,0))

        hits = self.tree.mapPoint((5,-0.001))
        self.failIf(len(hits))
        hits = self.tree.mapPoint((5,-0.001), 0.001)
        self.failUnless(len(hits) == 1)
        self.failUnless(hits[0][0] is self.c1)
        self.failUnless(hits[0][1] == (5,0))

if __name__ == "__main__":
    CLineTestSuite = unittest.TestSuite()
    CLineTestSuite.addTest(CLineTestCase("testCreation"))
    CLineTestSuite.addTest(CLineTestCase("testComparison"))
    CLineTestSuite.addTest(CLineTestCase("testPointMapping"))

    CLineTreeTestSuite = unittest.TestSuite()
    CLineTreeTestSuite.addTest(CLineTreeTestCase("testStorage"))
    CLineTreeTestSuite.addTest(CLineTreeTestCase("testBinscan"))
    CLineTreeTestSuite.addTest(CLineTreeTestCase("testScan"))
    CLineTreeTestSuite.addTest(CLineTreeTestCase("testRemove"))
    CLineTreeTestSuite.addTest(CLineTreeTestCase("testPointMapping"))

    unittest.main()
    
